package dao;

import factory.ConnectionFactory;
import modelo.Professor;
import java.sql.*;
import java.sql.PreparedStatement;

public class ProfessorDAO {
    private ConnectionFactory connection;
    long codigo;
    String nome;
    String rg;
    String cpf;
    String titulo;
    
    public ProfessorDAO(){
        this.connection = new ConnectionFactory();
    }
    public void adiciona(Professor professor){
        String sql = "INSER INTO professor(nome,rg,cpf,titulo) "
                + "VALUES ("+professor.getNome()+","
                + professor.getRg() + ","
                + professor.getCpf() + ","
                + professor.getTitulo() + ")";
       
        /* 
            Utilizar PreparedStatement para utilizar mesma sessão do banco para caso de 
            multiplos cadastros
        */
        connection.executeSQL(sql);
        connection.desconecta();
    }
}
