package dao;

import factory.ConnectionFactory;
import modelo.Curso;

public class CursoDAO {
    private ConnectionFactory connection;
    long codigo;
    String nome;
    String rg;
    String cpf;
    String titulo;
    
    public CursoDAO(){
        this.connection = new ConnectionFactory();
    }
    public void adiciona(Curso curso){
        String sql = "INSER INTO disciplina(descricao,duracao,periodo,qtd_alunos,carga_horaria) "
                + "VALUES ("+curso.getDescricao()+", "
                + curso.getDuracao() +", "
                + curso.getPeriodo() + ", "
                + curso.getQtdAluno() + ", "
                + curso.getCargaHoraria() +" )";
        /* 
            Utilizar PreparedStatement para utilizar mesma sessão do banco para caso de 
            multiplos cadastros
        */
        connection.executeSQL(sql);
        connection.desconecta();
    }

}
