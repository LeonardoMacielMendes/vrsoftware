package dao;

import factory.ConnectionFactory;
import modelo.Aluno;
import java.sql.*;
import java.sql.PreparedStatement;

public class AlunoDAO {
    private ConnectionFactory connection;
    long codigo;
    String nome;
    String rg;
    String cpf;
    String titulo;
    
    public AlunoDAO(){
        this.connection = new ConnectionFactory();
    }
    public void adiciona(Aluno aluno){
        String sql = "INSER INTO aluno(matricula,nome,rg,cpf) "
                + "VALUES ("+aluno.getMatricula()+", "
                + aluno.getNome()+", "
                + aluno.getRg() + ", "
                + aluno.getCpf() + " )";
        /* 
            Utilizar PreparedStatement para utilizar mesma sessão do banco para caso de 
            multiplos cadastros
        */
        connection.executeSQL(sql);
        connection.desconecta();
    }
    
}
