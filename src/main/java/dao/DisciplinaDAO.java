package dao;

import factory.ConnectionFactory;
import modelo.Disciplina;

public class DisciplinaDAO {
    private ConnectionFactory connection;
    long codigo;
    String nome;
    String rg;
    String cpf;
    String titulo;
    
    public DisciplinaDAO(){
        this.connection = new ConnectionFactory();
    }
    public void adiciona(Disciplina disciplina){
        String sql = "INSER INTO disciplina(descricao,ementa,limite_vaga,cod_prof,dia_semana,carga_horaria) "
                + "VALUES ("+disciplina.getDescricao()+", "
                + disciplina.getEmenta() +", "
                + disciplina.getLimiteVagas() + ", "
                + disciplina.getProfessor() + ", "
                + disciplina.getDiaSemana() +", "
                + disciplina.getCargaHoraria() +" )";
        /* 
            Utilizar PreparedStatement para utilizar mesma sessão do banco para caso de 
            multiplos cadastros
        */
        connection.executeSQL(sql);
        connection.desconecta();
    }

}
