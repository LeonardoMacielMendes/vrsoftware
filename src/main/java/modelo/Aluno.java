package modelo;

public class Aluno {
    int codigo;
    int matricula;
    String nome;
    String rg;
    String cpf;
    
    
    public int getCodigo(){
        return codigo;
    }
    public long getMatricula(){
        return matricula;
    }
    public String getNome(){
        return nome;
    }
    public String getRg(){
        return rg;
    }
    public String getCpf(){
        return cpf;
    }
    public void setCodigo(int codigo){
        this.codigo = codigo;
    }
    public void setMatricula(int matricula){
        this.matricula = matricula;
    }
    public void setNome(String nome){
        this.nome = nome;
    }
    public void setRg(String rg){
        this.rg = rg;
    }
    public void setCpf(String cpf){
        this.cpf = cpf;
    }
}
