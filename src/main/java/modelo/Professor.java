
package modelo;

public class Professor {
    long codigo;
    String nome;
    String rg;
    String cpf;
    String titulo;
    
    public long getCodigo(){
        return codigo;
    }
    public String getNome(){
        return nome;
    }
    public String getRg(){
        return rg;
    }
    public String getCpf(){
        return cpf;
    }
    public String getTitulo(){
        return titulo;
    }
    public void setCodigo(Long codigo){
        this.codigo = codigo;
    }
    public void setNome(String nome){
        this.nome = nome;
    }
    public void setRg(String rg){
        this.rg = rg;
    }
    public void setCpf(String cpf){
        this.cpf = cpf;
    }
    public void setTitulo(String titulo){
        this.titulo = titulo;
    }
}
