package factory;
import java.sql.Connection; 
import java.sql.SQLException; 
public class TestaConexao {     
    public static void main(String[] args) throws SQLException {
         ConnectionFactory connection = new ConnectionFactory();
         if (connection.getConnection()){
            System.out.println("Conexão aberta!");    
         }
         
         connection.desconecta();
     }
}