// situa em qual package ou "pacote" está a classe
package factory;
// faz as importações de classes necessárias para o funcionamento do programa 
import java.sql.Connection; 
// conexão SQL para Java 
import java.sql.DriverManager; 
// driver de conexão SQL para Java 
import java.sql.SQLException; 
// driver para interpretação do retorno do sql
import java.sql.ResultSet;
// driver para executar comandos no banco
import java.sql.Statement;
// driver para apresentar mensagem
import javax.swing.JOptionPane;

// classe para tratamento de exceções 
public class ConnectionFactory {
    // Variaveis de conexão
    final String driver = "org.postgresql.Driver";
    final String url = "jdbc:postgresql://localhost:5432/centroTreinamento";
    final String usuario = "postgres";
    final String senha = "postgres";
    Connection conexao;
    Statement s;
    ResultSet r;

    public boolean getConnection(){
        
        boolean result = true;
	try {
            // conecta com o banco
            Class.forName(driver);
            conexao = DriverManager.getConnection(url, usuario, senha);
	}     
	 catch (ClassNotFoundException ex) {
            JOptionPane.showMessageDialog(null, "Driver nao localizado: "+driver);
            result = false;
        }
        catch( SQLException Fonte) {
            result = false;
             String conecao = "Dejesa criar as tabelas do banco de dados?";
             int opcao_escolhida = JOptionPane.showConfirmDialog(null, conecao, " ", JOptionPane.YES_NO_OPTION);
	            if (opcao_escolhida == JOptionPane.YES_OPTION)
	            {
                        String sqlCreate = "CREATE DATABASE centroTreinamento";
	            	String sqlUse = "USE centroTreinamento";
                        String sqlProfessor = "CREATE TABLE professor (codigo serial not null)" +
                                "nome character varying(50) NOT NULL, rg integer not null," +
                                "cpf integer not null, titulo character varying(30) NOT NULL, "+
                                "CONSTRAINT codprof_pk PRIMARY KEY (codigo))";
                        String sqlAluno = "CREATE TABLE aluno (codigo serial not null)" +
                                "nome character varying(50) NOT NULL, rg integer not null," +
                                "cpf integer not null, matricula integer NOT NULL, "+
                                "CONSTRAINT codaluno_pk PRIMARY KEY (codigo))";
                        String sqlDisciplina = "CREATE TABLE disciplina (codigo serial not null)" +
                                "descricao character varying(20) NOT NULL,ementa character varying(50) NOT NULL," +
                                "limite_vaga integer not null, cod_prof integer NOT NULL, "+
                                "dia_semana character varying(20) NOT NULL, carga_horaria integer not null"+
                                "CONSTRAINT codaluno_pk PRIMARY KEY (codigo))";
                        String sqlDisciplinaCompl = "alter table aluno add constraint fk_disc_profe" +
                                "  foreign key(codprof) references professor(codigo) ";
                        String sqlCurso = "CREATE TABLE curso (codigo serial not null)" +
                                "descricao character varying(20) NOT NULL,duracao integer NOT NULL," +
                                "periodo character varying(20) NOT NULL, qtd_alunos integer NOT NULL, "+
                                "carga_horaria integer not null CONSTRAINT codaluno_pk PRIMARY KEY (codigo))";
                        try{
                            s.execute(sqlCreate);
                            s.execute(sqlUse);
                            s.execute(sqlProfessor);
                            s.execute(sqlAluno);
                            s.execute(sqlDisciplina);
                            s.execute(sqlDisciplinaCompl);
                            s.execute(sqlCurso);
                        } catch (SQLException e){
                            e.printStackTrace();
                        }
                    }
	}
        return result;
        
     }
    public void desconecta()
     {
         try 
         {
             conexao.close();
         }
         catch(SQLException Fonte)
         {
             JOptionPane.showMessageDialog(null, "Não foi possivel fechar o banco de dados");
         }
     }
     
     public void executeSQL(String sql)
     {
         try
         {
             s = conexao.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
             r = s.executeQuery(sql);                
         }
         catch(SQLException sqlex)
         {
             JOptionPane.showMessageDialog(null, "Não foi possivel "+
                     "executar o comando SQL"+sqlex);
         }
     }   

}
